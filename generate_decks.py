import genanki
import tomlkit
from toml_funcs import read_toml, strip_verifier

import subprocess
import argparse
from enum import Enum
from pathlib import Path

# for anki models
style = " .card { font-family: arial; font-size: 20px; text-align: center; color: black; background-color: white; }"

# order:
# question
# answer
# comment
# tags
# audio
# image


class note:
    def __init__(self, question, answer):
        self.question = question
        self.answer = answer
        self.with_reverse = False
        self.with_typing = False
        self.audio_path = ""  # if empty -> no audio
        self.comment = ""  # if empty -> no comment
        self.tags = []  # if empty -> no tags
        self.image_path = ""  # if empty -> no image

    def __repr__(self):
        return f"{self.question}: {self.answer}. reverse: {self.with_reverse}. typing: {self.with_typing}. audio: {self.audio_path}. comment: {self.comment}. tags: {self.tags}. image_path: {self.image_path}"


def generate_model(model_count, model_name, new_note, media_location):
    # generate the genanki model based on new_note
    model_fields = [{"name": "Front"}, {"name": "Back"}]
    # front
    if not new_note.with_typing:
        qfmt = "{{Front}}"
        afmt = "{{Front}}<br><br>{{Back}}"
    else:
        qfmt = "{{Front}}<br><br>{{type:Back}}"
        afmt = '{{Front}}<br><br>{{type:Back}}<br><br>{{Back}}<br><brb><input type="text" autofocus>'

    # comment
    comment_field = ""
    if new_note.comment != "":
        model_fields.append({"name": "Comment"})
        comment_field = "<br><br>{{Comment}}"

    # audio
    audio_field = ""
    if new_note.audio_path != "":
        model_fields.append({"name": "audio"}),
        audio_field = "<br><br>{{audio}}"

    # image_path
    image_field = ""
    if new_note.image_path != "":
        model_fields.append({"name": "image_path"})
        image_field = "<br><br>{{image_path}}"

    # generate first card
    templates = [
        {
            "name": "normal:" + model_name,
            "qfmt": qfmt + image_field,
            "afmt": afmt + comment_field + audio_field + image_field,
        },
    ]
    if new_note.with_reverse:
        if not new_note.with_typing:
            afmt = "{{Back}}"
            qfmt = "{{Back}}<br><br>{{Front}}"

        else:
            afmt = "{{Back}}<br><br>{{type:Front}}"
            qfmt = '{{Back}}<br><br>{{type:Front}}<br><br>{{Front}}<br><brb><input type="text" autofocus>'

        templates.append(
            {
                "name": "reversed:" + model_name,
                "qfmt": afmt + image_field,
                "afmt": qfmt + comment_field + audio_field + image_field,
            },
        )
    return genanki.Model(
        model_count, model_name, fields=model_fields, templates=templates, css=style
    )


def add_media_files_to_package(anki_package, media_location):
    p = Path(media_location)
    if p.is_dir():
        anki_package.media_files = list(p.glob("*"))
    return anki_package


def generate_audio_file(filename, to_read, media_location):
    # if media directory doesn't exist, create it
    Path(media_location).mkdir(parents=True, exist_ok=True)

    # if file with expected name already exists, don't regenerate it.
    # remove colons because ankimobile cant handle that
    filename = filename.replace(":", "")
    if "/" in filename or "\\" in filename or "[" in filename or "]" in filename:
        raise Exception(
            f"Cannot have characters: /, \\, [, ], in the card contents. if you wish to generate audio. Please remove them and retry: {filename}"
        )
    audio_file = Path(media_location + "/" + filename)
    if audio_file.is_file():
        pass
        # print(f"audio file {filename} exists, skipping")
    else:
        bashCommand = (
            'echo "'
            + to_read
            + '"|RHVoice-test -p Irina -o "'
            + media_location
            + "/"
            + filename
            + '"'
        )
        process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, shell=True)
        print(f"Generating audio file {filename}")
        output, error = process.communicate()
        print("done.")
        if error is not None:
            print(f"Error: {error}")


def generate_genanki_note(model, new_note, deck_name, media_location):
    card_fields = [new_note.question, new_note.answer]

    if new_note.comment != "":
        card_fields.append(new_note.comment)

    if new_note.audio_path != "":
        audio_path = deck_name + new_note.question + ".wav"

        audio_path = audio_path.replace(":", "")
        card_fields.append("[sound:" + audio_path + "]")

    if new_note.image_path != "":
        card_fields.append('<img src="' + new_note.image_path + '">')

    return genanki.Note(model=model, fields=card_fields, tags=new_note.tags)


def parse_anki_note(
    note_array, generated_models, model_count, deck_name, media_location
):
    # parses a toml note array into a genanki note. generates a new model if it
    # needs it. see README for the expected format,
    if len(note_array) < 2:
        raise Exception(f"toml line: {note_array} needs to have at least two elements.")
    new_note = note(note_array[0], note_array[1])  # question, answer
    new_note.tags = [] # default -> empty
    if len(note_array) > 2:
        for i in range(2, len(note_array)):
            if note_array[i].startswith("comment:"):
                new_note.comment = note_array[i].removeprefix("comment:")
            elif note_array[i].startswith("tags:"):
                note_array[i] = note_array[i].removeprefix("tags:").strip()
                new_note.tags = note_array[i].split(" ")
            elif note_array[i].startswith("image_path:"):
                new_note.image_path = note_array[i].removeprefix("image_path:")
            elif note_array[i] == "with_reverse":
                new_note.with_reverse = True
            elif note_array[i] == "with_typing":
                new_note.with_typing = True
            elif note_array[i] == "generate_audio":
                audio_path = deck_name + "::" + new_note.question + ".wav"
                generate_audio_file(audio_path, new_note.answer, media_location)
                new_note.audio_path = audio_path
            else:
                raise Exception(
                    f"toml line: {note_array} has an unknown field. review the expected format."
                )
    model_booleans = (
        [
            new_note.with_reverse,
            new_note.with_typing,
            (new_note.audio_path != ""),
            (new_note.comment != ""),
            (new_note.image_path != ""),
        ],
    )

    model_name = "".join(map(str, model_booleans))
    if model_name not in generated_models:
        generated_models[model_name] = generate_model(
            model_count, model_name, new_note, media_location
        )
        model_count += 1
    genanki_note = generate_genanki_note(
        generated_models[model_name],
        new_note,
        deck_name,
        media_location,
    )
    return genanki_note, generated_models, model_count


def recursive_toml_parse(
    data, generated_models, num=1, name="Automated Deck", media_location="media"
):
    anki_notes = genanki.Deck(num, name)
    decks = []
    num += 1  # for indexing decks and models

    # parse through table
    for d in data:
        a_d = data[d]  # the actual data
        s_d = d  # the string name of the data
        if isinstance(a_d, tomlkit.items.Table):
            recursed_decks, num, generated_models = recursive_toml_parse(
                a_d, generated_models, num, name + "::" + s_d, media_location
            )
            decks.extend(recursed_decks)
        elif isinstance(a_d, tomlkit.items.Array):
            for note_array in a_d:
                anki_note, generated_models, num = parse_anki_note(
                    note_array, generated_models, num, name, media_location
                )
                # if anki_note.model not in anki_notes.models:
                #    anki_notes.add_model(anki_note.model)
                anki_notes.add_note(anki_note)

        else:
            raise Exception(f"{d} is of unknown tomlkit type {type(data[d])}, exiting.")
    decks.append(anki_notes)
    return decks, num, generated_models


def debug(decks, generated_models):
    # use this when you need to debug what's generated
    for model_name in generated_models:
        print(model_name, generated_models[model_name].model_id)
    for deck in decks:
        print(f"DECK {deck.name}, {deck.deck_id}")
        print("MODELS")
        for model in deck.models:
            print(model)
        for note in deck.notes:
            print(f"note:{note.model.model_id}, {note.fields[0]}")
        print("-------------\n\n\n\n")


def main():
    parser = argparse.ArgumentParser(description="Parser for deck generation.")
    parser.add_argument(
        "input",
        type=str,
        default="input.TOML",
        help="The path to the input TOML file.",
    )
    parser.add_argument(
        "--output",
        "-o",
        type=str,
        default="output.apkg",
        help="The path to the output apkg file. If not provided, we will merely write to output.apkg",
    )
    parser.add_argument(
        "--medialocation",
        "-m",
        type=str,
        default="media",
        help="The path to the folder containing media",
    )
    args = parser.parse_args()
    toml = read_toml(args.input)
    if not strip_verifier(toml):
        print(
            "There are spaces to be stripped in your file. Take care of"
            "these before creating an anki deck. If you want to force"
            "the reaction of a deck anyway, use --f (not implemented yet)."
        )
    else:
        generated_models = dict()
        decks, _, _ = recursive_toml_parse(toml, generated_models)
        # debug(decks, generated_models)
        my_package = genanki.Package(decks)
        my_package = add_media_files_to_package(my_package, args.medialocation)
        my_package.write_to_file(args.output)
        print(f"Generated {args.output} .")


if __name__ == "__main__":
    main()
