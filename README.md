# What is this?

The following is a small set of scripts built on top of the python package
[genanki](https://github.com/kerrickstaley/genanki), for turning TOML lists of
decks into [Anki](https://apps.ankiweb.net/) packages. The intended use is for
automisation of deck creation, as I personally find the anki interface to be
rather time-consuming and frustrating to use. It supports multiple card types,
attachment of media, automatic generation of text-to-speech audio (for russian
only, so far, but you can edit the script super easily to support your software
of your choice)

# Dependencies
* [tomlkit](https://github.com/sdispater/tomlkit)
* [genanki](https://github.com/kerrickstaley/genanki)

#### Optional
* [RHVoice](https://github.com/RHVoice/RHVoice) (If you want automatic
  audio-generation of your cards)

# Usage

## Making your TOML card list
You're expected to give a TOML file containing a section for each deck. You can
have deeply-nested decks, that is, you can have decks that contain other decks.
To include notes in a deck, you will have to create a `cards` array, which
contains entries for each note, following the examples below. If you want to
look at an example, check out `example_deck.toml`.

Every note is an array which must include at least two strings, `["question",
"answer"]`. This will be the basis of your note, and will generate one basic
card from it, with, as you can guess, the question in the front, and the answer
in the back. It is *always* assumed that the first element's array is the
`question`, and that the second is the `answer`. You can then add the following
optional elements to your note's array:

* `"with_reverse"`: This will create a second card from your note, with the
  front and back sides reversed.
* `"with_typing"`: This will create a textbox within which to type the answer,
  and it will show what you got wrong when you flip the card. Useful for
  remembering spelling of words.
* `"comment:[some comment here]"`: This will let you add a comment which will
  be shown when you flip to the back side. Useful for adding a comment related
  to the expected answer, such as which grammar rule is employed.
* `"tags:[my_list of_tags here]"`: a space-delimited list of tags for the
  note. Here, the note has three tags: `my_list`, `of_tags`, and `here`. see
  Anki's 
  [guide on tags](https://apps.ankiweb.net/docs/manual20.html#adding-cards-and-notes) 
  in their manual for more information.

### Attaching or Generating Media
You can also add to your note's array options for denoting media. Note that
when dealing with media (generation, or usage), the default folder used will be
`media`, which python will look for wherever you call the script. if you want
to use some other media folder, you can give a path to the commandline optional
argument `--medialocation`.

* `"generate_audio"`: This will generate an audio file using `RHVoice` for this
  specific card. It will generate the audio for whichever `answer` you give to
  the card. Requires RHVoice.
* `"image_path:[my_image_path_here]"`: This will attach an image, shown during
  both the front and the back of the card. I may add an option to only show a
  card in the front or the back, but for now I haven't added it. You can only
  attach `1` image to your note.

## Using the scripts
* Use `generate_decks.py [my_toml_file]` to generate some decks. Check the
  `example_deck.toml` for a formatting guide, or read the section 
  [Making your TOML card list](#Making-your-TOML-card-list).
* Cleaning up a deck with orphaned notes with 
  `verifier.py [the_deck_toml_file] [the_txt_exported_file]`: Sometimes, you'll
  find yourself heavily editing the "keys" of notes , to the point where you
  have a number of leftover notes in the actual anki db, that don't match up to
  notes in your toml doc. It'd thus be useful to have a list of these
  "orphaned" notes, so that you may edit or remove them. To do this, do the
  following:
    - Export the deck you want to clean up in anki, using their export
      function: You want to export it as a .txt using the "Notes in text"
      function
    - use `verifier.py [the_deck_toml_file] [the_txt_exported_file]` You will
      get a list of cards back: These are all the notes in the .txt whose first
      element did not appear anywhere in the toml file.
 
# Helpful Tips
## Finding note duplicates
* In the top menu of anki -> Notes -> Find duplicates -> Front (the field
  used by these decks), add a filter for the deck in the text box below.

## Updating a Card Type for already existing notes 
Suppose you've made a large set of cards, but suddenly you wish to modify the
script to apply a change on the way some cards are implemented: for instance,
you decide to change the way comments are handled, or want to remove the input
html tag in the `typing` elements. Then, you need to:

* change the `generate_decks.py` file such that the changes you wish to apply
  are now in the card generation script.
* add new dummy cards in your `decks.toml` file, for every type of card that
  needs to be updated.
* run the `generate_decks.py` script on your `decks.toml`, and import it in
  your anki database: this will fail for every already existing card, but will
  succeed for importing your new dummy cards.
* In Browse, select all the cards with an existing type that needs to be
  updated (you can do this easily by filtering by card type, in the left
  sidebar, and then `CTRL-A`), then hit `CTRL-Shift-M` to enter the menu where
  you can change the card type. There, select the new card-type (which should
  only contain one card, the dummy card you made), and then `save`. You can now
  safely delete the old card type, and all your notes will have the changes you
  made in your new card type.

# Todo
* add a `-f/--force` option, and allow people to bypass the empty space checker.
* talk about characters that cant be handled well: (verify if we can handle them) [, ], /,
* make the logging be printed by a log, and have the `-v/--verbose` option
* change it such that the audio files are 1. generated by yandex, 2. dont contain colons because ankimobile cant handle that


