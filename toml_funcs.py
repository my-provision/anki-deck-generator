import tomlkit


def read_toml(toml_file):
    with open(toml_file, "r") as f:
        return tomlkit.parse(f.read())


def flatten_toml_decks(data):
    # given a parsed toml deck, flattens it to a list of cards,
    cards = []
    for d in data:
        a_d = data[d]  # the actual data
        if isinstance(a_d, tomlkit.items.Table):
            recursed_decks = flatten_toml_decks(a_d)
            cards.extend(recursed_decks)
        elif isinstance(a_d, tomlkit.items.Array):
            [cards.append(card) for card in a_d]
        else:
            raise Exception(f"{d} is of unknown type {type(data[d])} , exiting.")
    return cards


def strip_verifier(toml_data):
    # makes sure that you're passing a TOML file with no empty space to be
    # stripped in your entries . if there are some, returns false.
    data = flatten_toml_decks(toml_data)
    for card in data:
        for field in card:
            if field.strip() != field:
                print(f"Card {card} contains some whitespace")
                return False
    return True
