import sys
from toml_funcs import read_toml, flatten_toml_decks


def txt_parser(filename):
    cards = []
    with open(filename, "r") as f:
        [cards.append(line.split("\t")[0]) for line in f.readlines()]
    return cards


def main():
    if len(sys.argv) != 3:
        print(
            "You need to provide me two files: the first, a TOML file to parse."
            " The second, a .txt file to parse. Check the README for more help."
        )
    else:
        # only care about the first field of the card
        toml_cards = [card[0] for card in flatten_toml_decks(read_toml(sys.argv[1]))]
        txt_cards = txt_parser(sys.argv[2])
        print("List of cards I Couldn't find in your TOML file:")
        for card in txt_cards:
            if card not in toml_cards:
                print(card)


if __name__ == "__main__":
    main()
